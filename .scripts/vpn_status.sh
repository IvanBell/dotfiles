#!/bin/sh

if [ "$(pidof openvpn)" ]; then
    echo "VPN: ON"
else
    echo "VPN: OFF"
fi
